# Nintendo Switch Bootstack Documentation

This documentation is designed for developers. For a more high level overview for users, refer to the [switchroot wiki bootstack documentation](https://wiki.switchroot.org/wiki/linux/linux-bootstack-documentation)

The nintendo switch bootstack is provided partially by distros (Linux/Android) and partially by the nintendo switch bootloader [hekate](https://github.com/CTCaer/hekate)

## Distro Provided Bootfiles

`bl31.bin` - Arm Trusted Firmware (atf) - <https://gitlab.com/switchroot/bootstack/switch-atf>

`bl33.bin` - uboot - <https://gitlab.com/switchroot/bootstack/switch-uboot>

`boot.scr` - uboot script - not all on public repositories, Fedora's is here -> <https://gitlab.azka.li/l4t-community/gnu-linux/switchroot-pipeline/-/blob/master/scripts/bootloader/assets/boot.txt>

`initramfs` - initial root filesystem - not all on public repositories, example older L4T Bionic version -> <https://gitlab.com/switchroot/kernel/l4t-initramfs>

`nx-plat.dtimg` - combined device tree binaries for odin, modin, vali, and fric -\
<https://gitlab.com/switchroot/kernel/l4t-platform-tegra-common>\
<https://gitlab.com/switchroot/kernel/l4t-platform-t210-common>\
<https://gitlab.com/switchroot/kernel/l4t-soc-t210>\
<https://github.com/CTCaer/switch-l4t-platform-t210-nx>

`uImage` - linux kernel -\
<https://gitlab.com/switchroot/kernel/l4t-soc-tegra>\
<https://github.com/CTCaer/switch-l4t-kernel-nvidia>\
<https://gitlab.com/switchroot/kernel/l4t-kernel-nvgpu>\
<https://github.com/CTCaer/switch-l4t-kernel-4.9>

## Hekate Provided Bootfiles

`sc7entry.bin`: suspend to ram (sc7) entry firmware for t210 - decompiled and edited <https://gitlab.com/switchroot/bootstack/switch-coreboot/-/tree/switch-linux/src/soc/nvidia/tegra210/lp0/sc7_entry>

`sc7exit.bin`: suspend to ram (sc7) exit firmware for t210 - decompiled and edited
<https://gitlab.com/switchroot/bootstack/switch-coreboot/-/tree/switch-linux/src/soc/nvidia/tegra210/lp0/sc7_exit>

`sc7exit_b01.bin`: suspend to ram (sc7) exit firmware for t210b01 - nvidia provided and sourced from HOS

`bpmpfw.bin`: power management and includes ARC arachne register cell which does ram tables for overclock and also ram training for t210 - decompiled and edited, source not public

`bpmpfw_b01.bin`: power management, sc7 entry/exit, ram training/switching and includes ARC arachne register cell which does ram tables for overclock for t210b01 - decompiled and edited, source not public

`mtc_tbl_b01.bin`: ram tables for training from Nvidia/Nintendo for t210b01 - decompiled and edited, source not public

`libsys_minerva.bso`: ram training for t210 - <https://github.com/CTCaer/hekate/tree/master/modules/hekate_libsys_minerva>

`libsys_lp0.bso`: suspend to ram (sc7) pmc scratch register config - <https://github.com/CTCaer/hekate/tree/master/modules/hekate_libsys_lp0>